# ESMaven

## Build
mvn clean

mvn package

mvn clean compile assembly:single test

## Run
java -cp target/ESMaven-1.0.jar main.Main

java -cp target/ESMaven-1.0-jar-with-dependencies.jar main.Main

# MySQL
## Download
https://www.digitalocean.com/community/tutorials/how-to-install-the-latest-mysql-on-debian-10

## Configure user
https://www.digitalocean.com/community/tutorials/how-to-create-a-new-user-and-grant-permissions-in-mysql


## Maven + Java
https://mvnrepository.com/artifact/mysql/mysql-connector-java